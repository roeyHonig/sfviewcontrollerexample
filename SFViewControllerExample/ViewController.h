//
//  ViewController.h
//  SFViewControllerExample
//
//  Created by Roey Honig on 2.11.2019.
//  Copyright © 2019 Roey Honig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h>

@interface ViewController : UIViewController <SFSafariViewControllerDelegate>


@end

