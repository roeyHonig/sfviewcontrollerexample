//
//  AppDelegate.h
//  SFViewControllerExample
//
//  Created by Roey Honig on 2.11.2019.
//  Copyright © 2019 Roey Honig. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

