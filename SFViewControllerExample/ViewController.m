//
//  ViewController.m
//  SFViewControllerExample
//
//  Created by Roey Honig on 2.11.2019.
//  Copyright © 2019 Roey Honig. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)goToSafariBtnPreseed:(UIButton *)sender;
@property (strong, nonatomic) NSString *baseUrl;
@property (strong, nonatomic) SFSafariViewController *sFSafariViewController;
@property BOOL didJustRetrunedFromSfariViewController;

@end

@implementation ViewController
@synthesize baseUrl, sFSafariViewController, didJustRetrunedFromSfariViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    baseUrl = @"https://human-perception-system.com";
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"view did apear safari");
    if (didJustRetrunedFromSfariViewController) {
        NSLog(@"back from safari");
        didJustRetrunedFromSfariViewController = NO;
    }
}

- (void)startSafariVCWithUrl: (NSString *) siteAdress{
    NSURL *url = [[NSURL alloc] initWithString:siteAdress];
    if (url) {
        sFSafariViewController = [[SFSafariViewController alloc] initWithURL:url];
        sFSafariViewController.delegate = self;
        [self presentViewController:sFSafariViewController animated:YES completion:nil];
    } else {
        return;
    }
}

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller{
    didJustRetrunedFromSfariViewController = YES;
}

- (IBAction)goToSafariBtnPreseed:(UIButton *)sender {
    [self startSafariVCWithUrl:baseUrl];
}
@end
